<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_1</name>
   <tag></tag>
   <elementGuidId>6d884e11-8ded-4c96-ab22-c4296e875c18</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.shopping_cart_badge</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='shopping_cart_container']/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>87c53c3d-b717-4caf-98d0-6f107e110614</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>shopping_cart_badge</value>
      <webElementGuid>84135a25-7d6a-40ea-9139-0b80b988ac80</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>a166ea41-290d-4342-abfb-68bb9e05dc3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;shopping_cart_container&quot;)/a[@class=&quot;shopping_cart_link&quot;]/span[@class=&quot;shopping_cart_badge&quot;]</value>
      <webElementGuid>10cac53b-bacf-4a6e-b57b-fe35ce7225cf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='shopping_cart_container']/a/span</value>
      <webElementGuid>712cbe2b-d28d-46fa-905f-850249d57d03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Swag Labs'])[2]/following::span[1]</value>
      <webElementGuid>6a82039c-aee1-4e45-b70e-de584d5117f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close Menu'])[1]/following::span[1]</value>
      <webElementGuid>b4aa1f25-32d2-4e56-bbcc-d7cbd1ca9bce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Products'])[1]/preceding::span[1]</value>
      <webElementGuid>68b98e80-730c-4deb-bb93-28f86bd98f32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name (A to Z)'])[1]/preceding::span[2]</value>
      <webElementGuid>d10257ee-90a3-425f-949f-c010f89bd3e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span</value>
      <webElementGuid>46df9556-3bde-489e-9306-3ef6966eb7e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '1' or . = '1')]</value>
      <webElementGuid>7032c5a8-d160-46a3-84b1-932bc811303d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
