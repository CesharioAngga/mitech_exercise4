<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>testsuite1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b42e66a0-0a40-451a-bdac-56bb96fc5cf4</testSuiteGuid>
   <testCaseLink>
      <guid>1339b1ce-f473-4378-924e-67f456b8bdd7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCase1</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>13a7f062-3dfd-4fb1-b162-c2ad0d626287</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/data1</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>13a7f062-3dfd-4fb1-b162-c2ad0d626287</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>73ec11ab-cb19-4514-94f3-418c15bff1cf</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>13a7f062-3dfd-4fb1-b162-c2ad0d626287</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>ebb364c9-72b3-460e-b291-b4145418e66f</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
